//
//  ViewController.swift
//  Marriage Calculator
//
//  Created by Alish Giri on 11/4/18.
//  Copyright © 2018 Alish Giri. All rights reserved.
//

import UIKit

class AddPlayersVC: UIViewController {
    
    struct GlobalVariable {
        static var players = [String]()
        static var rounds = 0
        static var cashPerPoint = Double()
        static var currency = String()
    }
    
    @IBOutlet weak var lblAddedPlayers: UILabel!
    @IBOutlet weak var fldAddPlayers: UITextField!
    
    let vibrate = UIImpactFeedbackGenerator(style: .heavy)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fldAddPlayers.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if GlobalVariable.players.count > 0 {
            displayInLbl(msg: GlobalVariable.players.joined(separator: " "), type: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        fldAddPlayers.resignFirstResponder()
    }
    
    @IBAction func btnAddAction(_ sender: UIButton) {
        addPlayers()
    }
    
    func addPlayers() {
        vibrate.impactOccurred()
        if let player = fldAddPlayers.text {
            if player == "" {
                displayInLbl(msg: "Please enter a player name!!", type: "invalid")
                return
            }
            if GlobalVariable.players.contains(player) {
                displayInLbl(msg: "Player exist!\nPlease choose another name.", type: "invalid")
                return
            }
            GlobalVariable.players.append(player)
            if GlobalVariable.players.count == 6 {
                nextScreen()
                return
            }
            displayInLbl(msg: GlobalVariable.players.joined(separator: " "), type: nil)
        }
        fldAddPlayers.text = ""
    }
    
    @objc func nextScreen() {
        GlobalVariable.players.count >= 3
            ? performSegue(withIdentifier: "proceed", sender: nil)
            : displayInLbl(msg: "Not enough players!", type: "invalid")
    }
    
    @IBAction func btnDone(_ sender: UIButton) {
        nextScreen()
    }
    
    @IBAction func btnPlusIcon(_ sender: UIButton) {
        addPlayers()
    }
    
    func displayInLbl(msg: String, type: String?) {
        lblAddedPlayers.text = msg
        switch type {
        case "invalid":
            lblAddedPlayers.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        default:
            lblAddedPlayers.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    
}

extension AddPlayersVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        addPlayers()
        return true
    }
}

extension UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}
