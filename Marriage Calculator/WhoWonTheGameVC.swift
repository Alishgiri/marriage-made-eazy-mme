//
//  WhoWonTheGameVC.swift
//  Marriage Calculator
//
//  Created by Alish Giri on 11/5/18.
//  Copyright © 2018 Alish Giri. All rights reserved.
//

import UIKit

class WhoWonTheGameVC: UIViewController {
    
    var winner = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var positionY = 150
        for player in AddPlayersVC.GlobalVariable.players {
            let button = UIButton(frame: CGRect(x: 22, y: positionY, width: Int(self.view.frame.width) - 40, height: 50))
            button.setTitleColor(#colorLiteral(red: 0.03381565213, green: 0.5890541077, blue: 0.36511603, alpha: 1), for: [])
            button.titleLabel?.font = UIFont(name: "Futura-CondensedExtrabold", size: 30)
            button.setTitle(player, for: [])
            button.addTarget(self, action: #selector(playerConfirmed(sender:)), for: .touchUpInside)
            self.view.addSubview(button)
            positionY += 80
        }
    }
    
    @objc func playerConfirmed(sender: UIButton) {
        let vibrator = UIImpactFeedbackGenerator(style: .heavy)
        vibrator.impactOccurred()
        if let playWinner = sender.currentTitle {
            winner = playWinner
        }
        performSegue(withIdentifier: "winner", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is AddPlayersMaalVC {
            if let vc = segue.destination as? AddPlayersMaalVC {
                vc.winner = winner
            }
        }
    }
    
}
