//
//  FInalResultsVC.swift
//  Marriage Calculator
//
//  Created by Alish Giri on 11/5/18.
//  Copyright © 2018 Alish Giri. All rights reserved.
//

import UIKit

class FInalResultsVC: UIViewController {
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblLoser: UILabel!
    @IBOutlet weak var lblWinner: UILabel!
    @IBOutlet weak var lblDealToMake: UILabel!
    
    var totalMaal = 0
    var pointsToPay = [Int]()
    
    let players = AddPlayersVC.GlobalVariable.players
    let currency = AddPlayersVC.GlobalVariable.currency
    let cashPerPoint = AddPlayersVC.GlobalVariable.cashPerPoint
    let maalArr = AddPlayersMaalVC.Points.maal
    let winner = AddPlayersMaalVC.Points.winner
    
    let vibrator = UIImpactFeedbackGenerator(style: .heavy)
    
    var playerCount: Int! {
        didSet {
            playerCount == players.count ? setBtnTitle(title: "Play New Game") : setBtnTitle(title: "Next")
            if players.firstIndex(of: winner)! == players.count - 1 {
                playerCount == players.count - 1 ? setBtnTitle(title: "Play New Game") : setBtnTitle(title: "Next")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playerCount = 0
        for maal in maalArr {
            if maal >= 0 {
                totalMaal += maal
            }
            let pointsOwed = maal * players.count
            pointsToPay.append(pointsOwed)
        }
        calculate()
    }
    
    @IBAction func nextDealAction(_ sender: UIButton) {
        vibrator.impactOccurred()
        calculate()
    }
    
    func calculate() {
        if playerCount >= players.count {
            performSegue(withIdentifier: "newGame", sender: nil)
            return
        }
        if players[playerCount] == winner {
            playerCount += 1
            calculate()
            return
        }
        if pointsToPay[playerCount] >= 0 {
            let amount = Double((totalMaal + 3) - pointsToPay[playerCount])
            amount >= 0
                ? displayInlbl(loser: "\(players[playerCount])", amount: "\(currency) \(amount * cashPerPoint)", winner: "\(winner)")
                : displayInlbl(loser: "\(winner)", amount: "\(currency) \(abs(amount) * cashPerPoint)", winner: "\(players[playerCount])")
        } else {
            let amount = Double(totalMaal + 10)
            displayInlbl(loser: "\(players[playerCount])", amount: "\(currency) \(amount * cashPerPoint)", winner: "\(winner)")
        }
        playerCount += 1
    }
    
    func displayInlbl(loser: String, amount: String, winner: String) {
        lblLoser.text = loser
        lblDealToMake.text = amount
        lblWinner.text = winner
    }
    
    func setBtnTitle(title: String) {
        btnNext.setTitle(title, for: [])
    }
    
    @IBAction func startAgain(_ sender: UIButton) {
        vibrator.impactOccurred()
        AddPlayersMaalVC.Points.maal = []
        AddPlayersMaalVC.Points.winner = ""
    }
    
    @IBAction func resetGame(_ sender: UIButton) {
        vibrator.impactOccurred()
        AddPlayersMaalVC.Points.maal = []
        AddPlayersMaalVC.Points.winner = ""
        AddPlayersVC.GlobalVariable.players = []
        AddPlayersVC.GlobalVariable.rounds = 0
    }
    
}
