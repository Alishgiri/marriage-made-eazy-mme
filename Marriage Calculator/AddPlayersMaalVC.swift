//
//  AddPlayersMaalVC.swift
//  Marriage Calculator
//
//  Created by Alish Giri on 11/5/18.
//  Copyright © 2018 Alish Giri. All rights reserved.
//

import UIKit

class AddPlayersMaalVC: UIViewController {
    
    @IBOutlet weak var lblMaalOf: UILabel!
    @IBOutlet weak var btnUnseen: UIButton!
    @IBOutlet weak var fldMaal: UITextField!
    @IBOutlet weak var btnConfirmOutlet: UIButton!
    
    var winner = ""
    var individual: Int! {
        didSet {
            if individual <= players.count - 1 {
                displayInLbl(msg: "\(players[individual])'s maal?", type: nil)
                btnUnseen.isHidden = players[individual] == winner ? true : false
            } else {
                performSegue(withIdentifier: "calculate", sender: nil)
                return
            }
        }
    }
    
    let players = AddPlayersVC.GlobalVariable.players
    let vibrator = UIImpactFeedbackGenerator(style: UIImpactFeedbackGenerator.FeedbackStyle.heavy)
    
    struct Points {
        static var maal = [Int]()
        static var winner = String()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Points.winner = winner
        individual = 0
        displayInLbl(msg: "\(players[individual])'s maal?", type: nil)
        fldMaal.becomeFirstResponder()
    }
    
    @IBAction func btnSeenAction(_ sender: UIButton) {
        guard let maal = fldMaal.text else { return }
        if maal == "" {
            displayInLbl(msg: "Maal cannot be empty. Place 0 if player has no maal.", type: "red")
            return
        }
        if let point = fldMaal.text {
            addPlayersMaalLogic(point: Int(point)!)
        }
        print(players, Points.maal)
    }
    
    @IBAction func unSeenAction(_ sender: UIButton) {
        addPlayersMaalLogic(point: -1)
        vibrator.impactOccurred()
    }
    
    func addPlayersMaalLogic(point: Int) {
        Points.maal.append(point)
        fldMaal.text = ""
        individual += 1
        vibrator.impactOccurred()
    }
    
    func displayInLbl(msg: String, type: String?) {
        lblMaalOf.text = msg
        switch type {
        case "red":
            lblMaalOf.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        default:
            lblMaalOf.textColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
        }
    }
    
}

