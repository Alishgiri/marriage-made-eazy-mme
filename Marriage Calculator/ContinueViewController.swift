//
//  ContinueViewController.swift
//  Marriage Calculator
//
//  Created by Alish Giri on 11/6/18.
//  Copyright © 2018 Alish Giri. All rights reserved.
//

import UIKit

class ContinueViewController: UIViewController {
    
    @IBOutlet weak var lblRoundsOutlet: UILabel!
    
    var rounds = AddPlayersVC.GlobalVariable.rounds
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (rounds > 1) {
            lblRoundsOutlet.text = "Rounds: \(rounds)"
        }
    }
    
    @IBAction func btnContinue(_ sender: UIButton) {
        let vibrator = UIImpactFeedbackGenerator(style: .heavy)
        vibrator.impactOccurred()
        rounds += 1
    }
    
}
