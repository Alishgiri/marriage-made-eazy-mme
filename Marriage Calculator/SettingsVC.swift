//
//  SettingsVC.swift
//  Marriage Calculator
//
//  Created by Alish Giri on 11/14/18.
//  Copyright © 2018 Alish Giri. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var fldCurrency: UITextField!
    @IBOutlet weak var fldCashPerPoint: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fldCurrency.delegate = self
        fldCashPerPoint.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        saveValuesAutomatically()
    }
    
    func saveValuesAutomatically() {
        guard let currency = fldCurrency.text else { return }
        guard let cashPerPoint = fldCashPerPoint.text else { return }
        AddPlayersVC.GlobalVariable.currency = currency
        AddPlayersVC.GlobalVariable.cashPerPoint = Double(cashPerPoint) ?? 0.10
    }
}
